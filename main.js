/* DOM (Document Object Model)(Объектная модель документа) - наглядная модель документа,которая показывает всю его структуру и предоставляет
 возможность совершать с ней какие-то действия.
DOM – это представление HTML-документа в виде дерева тегов.
*/

let arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
// const ul = document.createElement("ul");

// function name(arr, ul) {
//   arr.map((item) => {
//     const li = document.createElement("li");
//     li.innerText = item;
//     ul.appendChild(li);
//   });
//   document.body.append(div);
//   div.append(ul);
//   document.body.append(arr.join("''"));
// }

// name(arr, ul);
function createList(arr, selector = document.body) {
  arr = arr.map(function callback(index) {
    return `<li>${index}</li>`;
  });
  let List = `<ul>${arr.join("")}</ul>`;
  selector.insertAdjacentHTML("beforeend", List);
}
createList(arr);
